import fs from "fs";
import { pipeline } from "stream";

const url = "http://10.5.5.9:8080/videos/DCIM/100GOPRO/";
const pathDownload = "./upload/";
const maxConcurrentDownloads = 5;
const downloadQueue = [];
let total = 0;
let count = 0;
let concurrentDownloads = 0;

const fetchFile = async (file) => {
  concurrentDownloads++;
  try {
    const { body, ok } = await fetch(url + file);

    if (!ok)
      throw new Error(
        `Failed to fetch ${file} - HTTP Status: ${response.status} ${response.statusText}`
      );

    if (!fs.existsSync(pathDownload)) {
      fs.mkdirSync(pathDownload);
    }
    const fileStream = fs.createWriteStream(pathDownload + file);

    return new Promise((resolve, reject) => {
      pipeline(body, fileStream, (err) => {
        if (err) {
          console.error(`Pipeline failed for file ${file}:`, err);
          reject(err);
        } else {
          count++;
          concurrentDownloads--;
          console.log(`Download Completed: ${file} (${count}/${total})`);
          resolve();
        }
      });
    });
  } catch (err) {
    console.error(`Error fetching ${file} : `, err);
  }
};

const fetchAll = async () => {
  try {
    const response = await fetch(url, { mode: "same-origin" });
    const html = await response.text();

    const regexp =
      /(?<=<tr><td><a href="\/videos\/DCIM\/100GOPRO\/)(.*)(?=">)/g;

    let m;
    while ((m = regexp.exec(html)) !== null) {
      const file = m[1];
      if (file && (file.includes(".JPG") || file.includes(".MP4"))) {
        downloadQueue.push(file);
      }
    }
    total = downloadQueue.length;

    while (downloadQueue.length && count < total) {
      if (concurrentDownloads < maxConcurrentDownloads) {
        const file = downloadQueue.shift();
        fetchFile(file);
      } else {
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  } catch (err) {
    console.error("Error fetching all files:", err);
  }
};

fetchAll();
